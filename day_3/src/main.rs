use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let filename = "input.txt";

    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    let mut tree_result : u32 = 1;
    let slopes = [(1,1),(3,1),(5,1),(7,1),(1,2)];
    

    let mut v = Vec::new();
    for line in reader.lines(){
        let line = line.unwrap();
        v.push(line);
    }
    
    
    for slope in slopes.iter(){
        let current_slope_right = slope.0;
        let current_slope_down = slope.1;
        let mut trees = 0;
        let mut current_col = 0;
        for i in (0..v.len()).step_by(current_slope_down){
            let tree_line : &String = &v[i];

            if tree_line.as_bytes()[current_col] as char == '#'{
                trees = trees + 1;
            }

            current_col = (current_col + current_slope_right) % tree_line.len();
        }
        tree_result = tree_result * trees;
    }
    println!("Trees: {}.", tree_result);
}


