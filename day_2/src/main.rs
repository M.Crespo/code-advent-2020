use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let filename = "input.txt";

    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);

    let mut number_valid_entries = 0;

    for (_, line) in reader.lines().enumerate(){
        let line = line.unwrap();
        let v: Vec<&str> = line.split(' ').collect();

        let first : Vec<&str> = v[0].split("-").collect();
        
        let first_index : usize = match first[0].parse() {
            Ok(number) => number,
            Err(_) => continue,
        };
        let first_index = first_index-1; 

        let last_index : usize = match first[1].parse() {
            Ok(number) => number,
            Err(_) => continue,
        };

        let last_index = last_index - 1;

        let letter = v[1].as_bytes()[0] as char;

        let string = v[2];

        if (string.chars().nth(first_index).unwrap() == letter && string.chars().nth(last_index).unwrap() != letter) 
        || (string.chars().nth(first_index).unwrap() != letter && string.chars().nth(last_index).unwrap() == letter){
            number_valid_entries = number_valid_entries + 1;
        }
    }
    println!("Number of valid entries: {}", number_valid_entries);
}
