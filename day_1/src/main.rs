use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let filename = "input.txt";

    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);

    let mut vec : Vec<u32> = Vec::new();

    for (_, line) in reader.lines().enumerate(){
        let line = line.unwrap();
        
        let line_num : u32 = match line.parse(){
            Ok(num) => num,
            Err(_) => continue,
        };

        vec.push(line_num);
    }

    for x in 0..vec.len() {
        for k in x+1..vec.len(){
            for i in k+1..vec.len(){
                if vec[x] + vec[k] + vec[i]== 2020{
                    println!("Result = {}.", vec[x] * vec[k] * vec[i]);
                    return;
                }
            }
        }
    }
}
